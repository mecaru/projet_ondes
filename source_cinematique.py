"""
Projet Ondes - Génération, propagation et détection des tsunamis.

Simulation de la source cinématique simplifiée du tsunamis par glissement de terrain.

Author : Karim

06/02/2020

TODO : Répartition de matière initiale plus réaliste
TODO : Création d'une fonction pour communiquer avec d'autres codes
"""
import matplotlib.pyplot as plt
import numpy as np
import time as time

# Paramètres utiles à la simulation
L0_depl = 0 # Longueur initiale de la dépletion
L0_accum = 4400 # Longueur initiale de la coulée
z0 = 25 # Épaisseur initiale de la coulée
Cl = 90 # Vitesse de déplacement de la frontière gauche de dépletion dans le sens des x décroissants
Cc = 90 # Vitesse de la frontière gauche de la coulée dans le sens des x croissants
Cr = 300 # Vitesse de la frontière droite de la coulée dans le sens des x croissants, doit être supérieure à Cc
tf = 100 # Durée de la simulation

# Animation
# Initialisation
plt.figure("Évolution de la source cinématique")
delta_t = 0 # Temps depuis le début de la simulation
t0 = time.time() # Date de l'instant initial
t = t0
x_lim = [-L0_depl-Cl*tf-1, L0_accum+Cr*tf+1] # Limites du graphe

# Tracé d'une figure toute les secondes
while time.time()-t0 < tf+1:
	# Début d'une itération
	if delta_t == 0 or time.time()-t > 0.2: 
		t = time.time()
		delta_t = t - t0
		L_accum = (Cr - Cc)*delta_t + L0_accum # Longueur de la coulée
		L_depl = (Cc + Cl)*delta_t + L0_depl # Longueur de la dépletion
		z_accum = z0*L0_accum/L_accum # Épaisseur de la coulée
		z_depl = L0_accum/L_depl*z0 # Profondeur de la dépletion
		pos = Cc*delta_t # Position de la frontière gauche de la coulée

		# Tracé
		plt.clf()
		plt.plot([pos]*2 + [pos+L_accum]*2, [0, z_accum, z_accum, 0], 'b') # Dessin de la coulée
		plt.plot([pos]*2 + [pos-L_depl]*2, [0, -z_depl, -z_depl, 0], 'b') # Dessin de la dépletion
		plt.plot(x_lim, 2*[0], 'r') #Dessin du sol 
		plt.text(0, 2*z0, "t = " + str(delta_t)) # Affichage du temps
		plt.xlim(x_lim[0], x_lim[1])
		plt.ylim(-3*z0, 3*z0)
		plt.grid()
		plt.pause(0.01)

plt.show()